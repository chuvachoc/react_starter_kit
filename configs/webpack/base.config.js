const path = require("path");
const webpack = require("webpack");
const LoaderOptionsPlugin = require("webpack/lib/LoaderOptionsPlugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const TsConfigPathsPlugin = require("tsconfig-paths-webpack-plugin");
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

const { alias } = require("./alias.config");

console.log("[config:webpack:snippet] Base loaded");

const pkg = require("../../package.json");

const proxy = {
  'development': 'http://localhost:3001',
  'production': 'http://localhost:3001',
}

console.log(`proxy to - ${proxy[process.env.NODE_ENV] || 'http://localhost:3001'}`);

module.exports = (env) => {
  const outputSuff = env.TS_TARGET === "es5" ? "es5.js": "mjs";

  console.log(`[config:webpack:snippet] Base: processing "${env.TS_TARGET}" config`);

  return {
    mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
    cache: true,
    devServer: {
      // http2: true,
      port: process.env.SERVE_PORT,
      contentBase: path.join(__dirname, "../../dist"),
      publicPath: "/assets/",
      writeToDisk: true,
      historyApiFallback: true,
      proxy: {
        '/api': {
          target: proxy[process.env.NODE_ENV] || 'http://localhost:3001'
        },
      }
    },
    entry: {
      "bundle": "./src/index.tsx",
    },
    resolve: {
      alias,
      extensions: [".js", ".jsx", ".html", ".ts", ".tsx", ".mjs"],
        modules: [
        "src",
        "node_modules",
      ],
      plugins: [
        new TsConfigPathsPlugin({
          configFile: path.join(__dirname, `../tsconfig.${env.TS_TARGET}.json`),
          logLevel: "info"
        })
      ]
    },
    output: {
      path: path.join(__dirname, "../../dist/static"),
      filename: `[name].${outputSuff}`,
      chunkFilename: `chunks/[name].${outputSuff}`,
      sourceMapFilename: `[name].${env.TS_TARGET}.map`,
      publicPath: "/static/",
    },
    plugins: [
      new ForkTsCheckerWebpackPlugin({
        typescript: {
          diagnosticOptions: {
            semantic: true,
            syntactic: true,
          },
        },
      }),
      new webpack.DefinePlugin({
        "process.env": {
          NODE_ENV: JSON.stringify(process.env.NODE_ENV),
          LOG_LEVEL: JSON.stringify(process.env.LOG_LEVEL),
          PKG_NAME: JSON.stringify(pkg.name),
          PKG_VERSION: JSON.stringify(pkg.version)
        },
      }),
      new LoaderOptionsPlugin({
        debug: process.env.NODE_ENV !== "production",
        minimize: process.env.NODE_ENV === "production"
      }),
      new CopyWebpackPlugin({
        patterns: [{
          from: "./src/assets",
          to: ".",
          globOptions: {
            ignore: [ "**/*.hbs", ".DS_Store" ],
          }
        }]
      }),
    ],
    node: false,
    watchOptions: {
      aggregateTimeout: 3000,
    }
  }
};
