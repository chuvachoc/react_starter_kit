const path = require("path");

function resolveBasePath(dir) {
  return path.join(__dirname, '../..', dir)
}

module.exports = {
  alias: {
    "@src": resolveBasePath("src")
  }
}
