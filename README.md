# React Application

## Installation

```bash
$ npm install
```

## Useful scripts

```bash
# run application
$ npm run start:dev

# check code quality
$ npm run lint

#open Cypress test tool
$ npm run cypress:open
```
