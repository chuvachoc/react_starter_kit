import React from "react";
import { Route, Switch } from "react-router-dom";
import { MainLoader } from "../components";
import { webAppRoutes } from "../constants";
import { PrivateRouter } from "./private-router";
import { PublicRouter } from "./public-router";

export function Routes({ loading = false }: { loading: boolean }): React.ReactElement {
  if (loading) {
    return <MainLoader />;
  }

  return (
    <Switch>
      <Route component={PublicRouter} path={webAppRoutes.user} />
      <Route component={PrivateRouter} path="/" />
    </Switch>
  );
}
