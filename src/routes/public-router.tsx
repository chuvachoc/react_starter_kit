import { MainLoader } from "@src/components/main-loader";
import { webAppRoutes } from "@src/constants";
import { MinimalLayout } from "@src/layouts";
import { NotFoundView } from "@src/pages/not-found";
import React, { Suspense } from "react";
import { Route, Switch } from "react-router-dom";
import { SignIn } from "./lazy-components";

export function PublicRouter(): JSX.Element {
  return (
    <MinimalLayout>
      <Suspense fallback={<MainLoader fullContent />}>
        <Switch>
          <Route component={SignIn} exact path={webAppRoutes.signIn} />

          <Route path="*">
            <NotFoundView />
          </Route>
        </Switch>
      </Suspense>
    </MinimalLayout>
  );
}
