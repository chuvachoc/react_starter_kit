import React from "react";
import { DefaultLazyResult, LazyComponentModel } from "../typings";

// offers
export const Dashboard = React.lazy(
  (): LazyComponentModel => import("../pages/dashboard").then((m): DefaultLazyResult => ({ default: m.Dashboard }))
);

// authentication
export const SignIn = React.lazy(
  (): LazyComponentModel => import("../pages/sign-in").then((m): DefaultLazyResult => ({ default: m.SignIn }))
);
