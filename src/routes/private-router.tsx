import { GuardedRoute, MainLoader } from "@src/components";
import { webAppRoutes } from "@src/constants";
import { MainLayout } from "@src/layouts";
import { NotFoundView } from "@src/pages/not-found";
import { InterceptorService } from "@src/services/interceptrors-service";
import React, { Suspense } from "react";
import { RouteComponentProps } from "react-router";
import { Switch } from "react-router-dom";
import * as pages from "./lazy-components";

export function PrivateRouter({ history }: RouteComponentProps): JSX.Element {
  InterceptorService.setHistory(history);

  return (
    <MainLayout>
      <Suspense fallback={<MainLoader fullContent />}>
        <Switch>
          <GuardedRoute component={pages.Dashboard} exact path={webAppRoutes.home} />

          <GuardedRoute component={NotFoundView} path="*" />
        </Switch>
      </Suspense>
    </MainLayout>
  );
}
