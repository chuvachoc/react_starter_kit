/** @jsx jsx */
import { jsx } from "@emotion/core";
import React from "react";
import { Link as RouterLink } from "react-router-dom";
import { AppBar, Toolbar } from "@material-ui/core";

import { useStyles } from "./TopBar.styles";

type TopBarProps = {
  className?: string;
};

const defaultProps: TopBarProps = {
  className: "",
};

export function TopBar(props: TopBarProps = defaultProps): React.ReactElement {
  const { className } = props;

  const styles = useStyles();

  return (
    <AppBar css={styles} className={className} color="primary" position="fixed">
      <Toolbar>
        <RouterLink className="link" to="/">
          <img className="logo" alt="React App" src="/static/images/logos/logo.png" />
        </RouterLink>
      </Toolbar>
    </AppBar>
  );
}
