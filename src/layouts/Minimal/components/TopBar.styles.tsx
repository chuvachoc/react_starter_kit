import { css, SerializedStyles } from "@emotion/core";

export const useStyles = (): SerializedStyles => css`
  box-shadow: none;

  .link {
    display: flex;
  }

  .logo {
    width: 45px;
    height: 45px;
  }
`;
