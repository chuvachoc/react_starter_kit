/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import React, { PropsWithChildren, ReactElement } from "react";
import { IPlainObject } from "@src/models";

import { TopBar } from "./components";

const styles = css`
  padding-top: 64px;
  height: 100%;

  .content {
    height: 100%;
  }
`;

export function MinimalLayout<P = IPlainObject>({ children }: PropsWithChildren<P>): ReactElement {
  return (
    <div css={styles}>
      <TopBar />
      <main className="content">{children}</main>
    </div>
  );
}
