/** @jsx jsx */
import { jsx } from "@emotion/core";
import { useTheme } from "@material-ui/styles";
import { notification } from "@src/constants";
import { IPlainObject } from "@src/models";
import { SignOut, usePopupAlertsReducer } from "@src/store/reducers";
import { ThemeProps } from "@src/theme/mui-theme";
import React, { PropsWithChildren, useEffect } from "react";
import { useDispatch } from "react-redux";
import { TopBar } from "./components";
import { useStyles } from "./MainLayout.styles";

export function MainLayout<P = IPlainObject>({ children }: PropsWithChildren<P>): React.ReactElement {
  const theme = useTheme<ThemeProps>();
  const styles = useStyles(theme);
  const { showAlert } = usePopupAlertsReducer();
  const dispatch = useDispatch();

  useEffect(() => {
    showAlert({
      content: notification.signedOutSuccessfully,
    });

    dispatch(new SignOut());
  }, []);

  return (
    <div css={styles} data-test-id="main-layout">
      <TopBar />
      <div className="wrapper">
        <div className="content-container">
          <div className="content">{children}</div>
        </div>
      </div>
    </div>
  );
}
