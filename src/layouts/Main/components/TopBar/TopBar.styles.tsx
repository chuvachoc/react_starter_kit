import { css, SerializedStyles } from "@emotion/core";
import { ThemeProps } from "@src/theme/mui-theme";

export const useStyles = (theme: ThemeProps): SerializedStyles => css`
  box-shadow: none;

  .flex-grow {
    flex-grow: 1;
    display: flex;
    padding: 0 24px;

    & > a {
      color: ${theme.palette.white};
      font-weight: 500;
      font-size: 14px;
      font-family: "Roboto", "Helvetica", "Arial", sans-serif;
      line-height: 1.75;
      border-radius: 4px;
      letter-spacing: 0.02857em;
      text-transform: uppercase;

      &:not(:last-of-type) {
        margin-right: 24px;
      }
    }
  }

  .sign-out-button {
    margin-left: ${theme.spacing(1)}px;
  }

  .link {
    display: flex;
  }

  .logo {
    width: 45px;
    height: 45px;
  }

  .header-button,
  .page-button {
    padding: 20px 8px;
    max-width: 200px;
    color: ${theme.palette.white};

    &.Mui-disabled {
      color: ${theme.palette.primary.light};
    }

    .MuiButton-label {
      white-space: nowrap;
      display: inline-block;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  }

  .page-button {
    &.active {
      position: relative;

      :after {
        content: "";
        height: 2px;
        width: 100%;
        background-color: ${theme.palette.white};
        bottom: 0;
        left: 0;
        position: absolute;
      }
    }
    &:hover {
      background-color: rgba(0, 0, 0, 0.03);
    }

    .MuiTouchRipple-root {
      display: none;
    }
  }
`;
