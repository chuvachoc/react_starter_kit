/** @jsx jsx */
import { jsx } from "@emotion/core";
import { AppBar, Badge, Button, Hidden, IconButton, Toolbar } from "@material-ui/core";
import InputIcon from "@material-ui/icons/Input";

import NotificationsIcon from "@material-ui/icons/NotificationsOutlined";
import { useTheme } from "@material-ui/styles";
import { selectUser } from "@src/store/reducers";
import { ThemeProps } from "@src/theme/mui-theme";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Link as RouterLink } from "react-router-dom";
import { useStyles } from "./TopBar.styles";

type TopBarProps = {
  className?: string;
  onSignOut?: () => void;
};

export function TopBar(props: TopBarProps): React.ReactElement {
  const { className, onSignOut } = props;

  const theme = useTheme<ThemeProps>();
  const styles = useStyles(theme);
  const user = useSelector(selectUser);

  const [notifications] = useState([]);

  return (
    <AppBar css={styles} className={className} data-test-id="main-top-bar">
      <Toolbar data-test-id="main-top-bar-container">
        <RouterLink className="link" to="/">
          <img className="logo" alt="React App" src="/static/images/logos/logo.png" data-test-id="main-logo" />
        </RouterLink>
        <div className="flex-grow" data-test-id="app-bar-buttons"></div>
        <Hidden smDown>
          <IconButton color="inherit">
            <Badge badgeContent={notifications.length} color="primary" variant="dot">
              <NotificationsIcon />
            </Badge>
          </IconButton>
          <IconButton className="sign-out-button" color="inherit" onClick={onSignOut} data-test-id="sign-off-button">
            <InputIcon />
          </IconButton>
        </Hidden>
        <Hidden smDown>
          {user && (
            <Button disabled={!user} className="header-button">
              {user.name}
            </Button>
          )}
        </Hidden>
      </Toolbar>
    </AppBar>
  );
}
