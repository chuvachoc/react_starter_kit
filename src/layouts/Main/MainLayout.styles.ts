import { css, SerializedStyles } from "@emotion/core";
import { ThemeProps } from "@src/theme/mui-theme";

export const useStyles = (theme: ThemeProps): SerializedStyles => css`
  background-color: ${theme.palette.background.dark};
  display: flex;
  overflow: hidden;
  width: 100%;
  height: 100%;

  .wrapper {
    display: flex;
    flex: 1 1 auto;
    overflow: hidden;
    padding-top: ${theme.spacing(8)}px;

    ${theme.breakpoints.up("md")} {
      padding-left: 240px;
    }
  }
  .content-container {
    display: flex;
    flex: 1 1 auto;
    overflow: hidden;
  }
  .content {
    flex: 1 1 auto;
    height: 100%;
    overflow: auto;
    padding: ${theme.spacing(3)}px;
  }
`;
