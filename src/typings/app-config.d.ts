import { AppEnvConfigModel } from "../models/app-env-config";

export interface AppConfig {
  apiBaseUrl: string;
  isNode: boolean;
  name: string;
  startApp: boolean;
  version: string;
  env: AppEnvConfigModel;
}
