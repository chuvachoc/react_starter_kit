export * from './app-config';
export * from './lazy-load';

declare global {
  const PKG_NAME: string;
  const PKG_VERSION: string;

  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: Function;
    __REDUX_DEVTOOLS_EXTENSION__: any;
    __CYPRESS_REDUX_STORE__: any;
    Cypress: any;
  }
}

declare module '*.css' {
  interface ClassNames {
    [className: string]: string
  }

  const classNames: ClassNames;
  export = classNames;
}
