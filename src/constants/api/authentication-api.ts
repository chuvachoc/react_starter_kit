const AUTHENTICATION_URL = "/authentication";

export function authenticationApi(): string {
  return AUTHENTICATION_URL;
}

export function signInApi(): string {
  return `${AUTHENTICATION_URL}/log-in`;
}

export function signOutApi(): string {
  return `${AUTHENTICATION_URL}/log-out`;
}
