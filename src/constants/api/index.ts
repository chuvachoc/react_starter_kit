import { authenticationApi, signInApi, signOutApi } from "./authentication-api";

export const serviceEndpoints = {
  authenticationApi,
  signInApi,
  signOutApi,
};
