export enum JoiAppErrorCode {
  StringPatternBase = "string.pattern.base",
  StringPatternName = "string.pattern.name",
  StringMin = "string.min",
  StringBase = "string.base",
  AnyOnly = "any.only",
}
