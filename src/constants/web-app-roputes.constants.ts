export enum RouteKeys {
  homePage = "home",
  user = "user",
}

export const BASE_ROUTE = "/";

export const baseRoutes = {
  user: `/${RouteKeys.user}`,
};

export const webAppRoutes = {
  home: BASE_ROUTE,
  notFound: "/404",

  user: baseRoutes.user,
  signIn: `${baseRoutes.user}/sign-in`,
};
