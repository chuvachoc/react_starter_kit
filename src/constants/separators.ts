export const separators = {
  twoPoints: ":",
  slash: "/",
  space: " ",
  emptyNumber: "-",
};
