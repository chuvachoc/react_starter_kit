export enum PopupAlertType {
  Info = "info",
  Warning = "warning",
  Error = "error",
  Success = "success",
}
