import { AppConfig } from "../typings";

export const appConfig: AppConfig = {
  apiBaseUrl: "/api/",
  name: process.env.PKG_NAME,
  isNode: false,
  startApp: true,
  version: process.env.PKG_VERSION,
  env: {
    LOG_LEVEL: null,
    NODE_ENV: null,
    PKG_NAME: null,
    PKG_VERSION: null,
    ...process.env,
  },
};
