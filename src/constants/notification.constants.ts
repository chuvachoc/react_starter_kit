export const notification = {
  signedInSuccessfully: (firstName: string): string => `Dear ${firstName}! You are successfully signed in!`,
  signedOutSuccessfully: `You are signed out successfully!`,
};
