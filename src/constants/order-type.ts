export enum OrderType {
  Asc = "ASC",
  Desc = "DESC",
}

export const orderTypeLabel = {
  [OrderType.Asc]: "asc",
  [OrderType.Desc]: "desc",
};
