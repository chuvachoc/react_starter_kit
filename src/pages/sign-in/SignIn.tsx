/** @jsx jsx */
import { jsx } from "@emotion/core";
import { Grid, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/styles";
import { ThemeProps } from "@src/theme/mui-theme";
import React from "react";

import { SignInForm } from "./components/SignInForm/SignInForm";

import { useStyles } from "./SignIn.styles";

export function SignIn(): React.ReactElement {
  const theme = useTheme<ThemeProps>();
  const styles = useStyles(theme);

  return (
    <div css={styles}>
      <Grid className="grid" container>
        <Grid>
          <div className="content">
            <Typography variant="h2" id="sign-in-title">
              Sign in
            </Typography>

            <Typography id="sign-in-subtitle" className="marginTop2" color="textSecondary" variant="body1">
              login with email address
            </Typography>

            <SignInForm />
          </div>
        </Grid>
      </Grid>
    </div>
  );
}
