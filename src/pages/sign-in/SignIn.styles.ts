import { css, SerializedStyles } from "@emotion/core";
import { ThemeProps } from "@src/theme/mui-theme";

export const useStyles = (theme: ThemeProps): SerializedStyles => css`
  flex-basis: 300px;

  ${theme.breakpoints.down("sm")} {
    padding: 0 ${theme.spacing(2)};
  }

  .marginTop2 {
    margin-top: ${theme.spacing(2)};
  }

  background-color: ${theme.palette.background.default};
  height: 100%;

  .grid {
    height: 100%;
    align-items: center;
    justify-content: center;
    padding: ${theme.spacing(3)}px;
  }

  .content {
    height: 100%;
    display: flex;
    flex-direction: column;

    .body {
      margin-top: -${theme.spacing(2)}px;
      flex-grow: 1;
      display: flex;
      justify-content: center;
      align-items: center;
      ${theme.breakpoints.down("md")} {
        justify-content: center;
      }
    }
  }
`;
