import { css, SerializedStyles } from "@emotion/core";
import { ThemeProps } from "@src/theme/mui-theme";

export const useStyles = (theme: ThemeProps): SerializedStyles => css`
  .text-field {
    margin-top: ${theme.spacing(2)}px;

    .MuiFormHelperText-root {
      color: ${theme.palette.error.main};
    }
  }

  .sign-in-button {
    margin: ${theme.spacing(2, 0)};
  }
`;
