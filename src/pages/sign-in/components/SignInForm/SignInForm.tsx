/** @jsx jsx */
import { jsx } from "@emotion/core";
import { joiResolver } from "@hookform/resolvers/joi";
import { Button, TextField } from "@material-ui/core";
import { useTheme } from "@material-ui/styles";
import { webAppRoutes } from "@src/constants";
import { notification } from "@src/constants/notification.constants";

import { UserModel } from "@src/models";
import { selectUser, SignIn, usePopupAlertsReducer } from "@src/store/reducers";
import { ThemeProps } from "@src/theme/mui-theme";
import React, { useEffect } from "react";
import { Controller, FormProvider, useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { signInFormDefaultValues, signInFormSchema } from "./SignInForm.shemas";

import { useStyles } from "./SignInForm.styles";

export function SignInForm(): React.ReactElement {
  const history = useHistory();
  const theme = useTheme<ThemeProps>();
  const styles = useStyles(theme);
  const dispatch = useDispatch();
  const { showAlert } = usePopupAlertsReducer();

  const user = useSelector(selectUser);

  const formHook = useForm({
    defaultValues: { ...signInFormDefaultValues },
    mode: "all",
    reValidateMode: "onChange",
    resolver: joiResolver(signInFormSchema),
  });

  const signIn = (data) => {
    showAlert({
      content: notification.signedInSuccessfully("User Name"),
    });

    dispatch(new SignIn(data as UserModel));
  };

  useEffect(() => {
    if (!user) return;

    history.push(webAppRoutes.home);
  }, [user]);

  return (
    <FormProvider {...formHook}>
      <form onSubmit={formHook.handleSubmit(signIn)} css={styles}>
        <Controller
          name="email"
          render={({ value, onChange, onBlur }) => (
            <TextField
              className="text-field"
              data-test-id="email-text-field"
              error={!!formHook.formState.errors.email}
              fullWidth
              helperText={formHook.formState.errors.email?.message || " "}
              label="User code"
              name="email"
              onChange={onChange}
              onBlur={onBlur}
              type="text"
              value={value}
              variant="outlined"
            />
          )}
        />
        <Controller
          name="password"
          render={({ value, onChange, onBlur }) => (
            <TextField
              className="text-field"
              data-test-id="password-text-field"
              fullWidth
              error={!!formHook.formState.errors.password}
              helperText={formHook.formState.errors.password?.message || " "}
              label="Password"
              name="password"
              onChange={onChange}
              onBlur={onBlur}
              type="password"
              value={value}
              variant="outlined"
            />
          )}
        />
        <Button
          className="sign-in-button"
          data-test-id="sign-in-button"
          color="primary"
          disabled={!formHook.formState.isValid || !formHook.formState.isDirty}
          fullWidth
          size="large"
          type="submit"
          variant="contained"
        >
          Sign in now
        </Button>
      </form>
    </FormProvider>
  );
}
