import joi from "joi";

export interface SignInFormSchema {
  email: string;
  password: string;
}

export const signInFormDefaultValues = <SignInFormSchema>{
  email: "",
  password: "",
};

export const signInFormSchema = joi.object<SignInFormSchema>({
  email: joi
    .string()
    .trim()
    .email({ tlds: { allow: false } }),
  password: joi.string().trim().required(),
});
