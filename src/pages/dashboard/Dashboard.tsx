/** @jsx jsx */
import { jsx } from "@emotion/core";
import { useTheme } from "@material-ui/styles";
import { selectUser, SignIn } from "@src/store/reducers";
import { ThemeProps } from "@src/theme";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useDashboardStyles } from "./Dashboard.styles";

export function Dashboard(): React.ReactElement {
  const theme = useTheme<ThemeProps>();
  const style = useDashboardStyles(theme);
  const user = useSelector(selectUser);

  return (
    <div data-test-id="offers-container" css={style}>
      <div>Dashboard</div>
      <div>user: {user && user.name}</div>
    </div>
  );
}
