import { css, SerializedStyles } from "@emotion/core";
import { ThemeProps } from "@src/theme/mui-theme";

export const useDashboardStyles = (theme: ThemeProps): SerializedStyles => css`
  .active-state {
    color: ${theme.palette.warning.main};
  }
  .inactive-state {
    color: ${theme.palette.success.main};
  }
`;
