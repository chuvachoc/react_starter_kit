import { css, SerializedStyles } from "@emotion/core";

export const useStyles = (): SerializedStyles => css`
  flex-basis: 500px;
`;
