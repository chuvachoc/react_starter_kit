/** @jsx jsx */
import { jsx } from "@emotion/core";
import { Snackbar, SnackbarOrigin } from "@material-ui/core";
import Slide from "@material-ui/core/Slide";
import { TransitionProps } from "@material-ui/core/transitions";
import Alert from "@material-ui/lab/Alert";
import { PopupAlertType } from "@src/constants";

import { PopupAlertState, selectAllPopupAlerts } from "@src/store/reducers";
import { is } from "@src/utils/is";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";

import { useStyles } from "./PopupAlert.styles";

export interface State extends SnackbarOrigin {
  open: boolean;
  alert: PopupAlertState;
  Transition: React.ComponentType<TransitionProps & { children?: React.ReactElement }>;
}

function SlideTransition(props: TransitionProps) {
  return <Slide {...props} direction="down" />;
}

export function PopupAlert(): React.ReactElement {
  const styles = useStyles();
  const alerts = useSelector(selectAllPopupAlerts);

  const [state, setState] = React.useState<State>({
    open: false,
    vertical: "top",
    horizontal: "center",
    Transition: SlideTransition,
    alert: { variant: PopupAlertType.Info, content: "" },
  });

  const { vertical, horizontal, open, alert } = state;

  const handleClose = () => {
    setState({ ...state, open: false });
  };

  useEffect(() => {
    if (is.empty(alerts.content)) return;

    setState({
      ...state,
      open: true,
      alert: alerts,
    } as State);
  }, [alerts]);

  return (
    <Snackbar
      css={styles}
      data-test-id="snackbar-container"
      anchorOrigin={{ vertical, horizontal }}
      open={open}
      onClose={handleClose}
      key={state.Transition.name}
      autoHideDuration={20000}
      TransitionComponent={state.Transition}
    >
      <Alert severity={alert.variant} onClose={handleClose}>
        {alert.content}
      </Alert>
    </Snackbar>
  );
}
