import React from "react";
import { Route, RouteProps } from "react-router-dom";

export function GuardedRoute(props: RouteProps): React.ReactElement {
  const { component, ...rest } = props;
  const Component = component as React.ElementType;

  return <Route {...rest} render={(matchProps) => <Component {...matchProps} />} />;
}
