/** @jsx jsx */
import { jsx } from "@emotion/core";
import { cn } from "@src/utils/cn";
import React from "react";
import { CircularProgress, Grid, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { ThemeProps } from "@src/theme/mui-theme";

import { useStyles } from "./MainLoader.styles";

interface MainLoaderProps {
  block?: boolean;
  fullScreen?: boolean;
  fullContent?: boolean;
  fillContainer?: boolean;
}

export function MainLoader({ block, fullScreen, fillContainer, fullContent }: MainLoaderProps): React.ReactElement {
  const theme = useTheme<ThemeProps>();
  const styles = useStyles(theme);

  return (
    <div css={styles}>
      <div
        className={cn("loader", {
          overlay: !block,
          "full-screen": fullScreen,
          "full-content": fullContent,
          "fill-container": fillContainer,
        })}
      >
        <Grid className="grid" container>
          <Grid className="content" item md={12} lg={12}>
            <CircularProgress color="inherit" />
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
