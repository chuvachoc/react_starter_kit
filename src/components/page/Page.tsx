import React, { FC, forwardRef, MutableRefObject, PropsWithChildren } from "react";
import { Helmet } from "react-helmet";

interface PageProps {
  title: string;
  className: string;
}

export const Page: FC<PropsWithChildren<PageProps>> = forwardRef(
  ({ children, title = "", className }, ref: ((instance: null) => void) | MutableRefObject<null> | null) => {
    return (
      <div ref={ref} className={className}>
        <Helmet>
          <title>{title}</title>
        </Helmet>
        {children}
      </div>
    );
  }
);
