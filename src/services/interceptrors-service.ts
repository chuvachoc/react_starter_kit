import { ServerStatusCode, webAppRoutes } from "@src/constants";
import Axios from "axios";
import * as H from "history";
import { AppConfig } from "../typings";

export class InterceptorService {
  private static history: H.History;

  public static init(appConfig: AppConfig): void {
    Axios.defaults.baseURL = appConfig.apiBaseUrl;

    InterceptorService.addResponseInterceptor();
  }

  public static setHistory(history: H.History): void {
    this.history = history;
  }

  private static addResponseInterceptor(): void {
    Axios.interceptors.response.use(undefined, (error) => {
      if (error && error.isAxiosError && error.response.status === ServerStatusCode.NOT_FOUND && this.history) {
        setTimeout(() => {
          this.history.push(webAppRoutes.notFound);
        }, 200);
      }

      return Promise.reject(error);
    });
  }
}
