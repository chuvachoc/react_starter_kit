import { IPlainObject } from "../models";

const STORAGE_KEY = "reactApp";

export class LocalStorageService {
  static clearState(): void {
    window.localStorage.clear();
  }

  static setState(data: IPlainObject): void {
    window.localStorage.setItem(STORAGE_KEY, JSON.stringify(data));
  }

  static getState(): IPlainObject {
    const jsonState = window.localStorage.getItem(STORAGE_KEY);

    if (!jsonState) {
      return null;
    }

    return JSON.parse(jsonState);
  }
}
