export interface AppEnvConfigModel {
  LOG_LEVEL: string;
  NODE_ENV: string;
  PKG_NAME: string;
  PKG_VERSION: string;
}
