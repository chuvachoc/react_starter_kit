export enum UserRole {
  Admin = "admin",
  Manager = "manager",
  Moderator = "moderator",
}

export interface UserBaseModel {
  email: string;
  name: string;
  description: string;
  role: UserRole;
}

export type UserModel = UserBaseModel;
