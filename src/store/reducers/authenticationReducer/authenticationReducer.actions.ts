import { UserModel } from "@src/models";

export enum AuthenticationActionTypes {
  SIGN_IN = "[Authentication] SIGN_IN",
  SIGN_OUT = "[Authentication] SIGN_OUT",
}

export class SignIn {
  public readonly type = AuthenticationActionTypes.SIGN_IN;

  constructor(public user: UserModel = null) {}
}

export class SignOut {
  public readonly type = AuthenticationActionTypes.SIGN_OUT;
}

export type AuthenticationActions = SignIn | SignOut;
