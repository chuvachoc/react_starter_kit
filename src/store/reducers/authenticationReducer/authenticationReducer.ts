import { UserModel, UserRole } from "@src/models";
import { AuthenticationActions, AuthenticationActionTypes } from "./authenticationReducer.actions";

export interface AuthenticationState {
  user: UserModel;
}

const initialState = <AuthenticationState>{
  user: null,
};

interface AppStateWithAuthentication {
  authenticationReducer: AuthenticationState;
}

export function authenticationReducer(state = initialState, action: AuthenticationActions): AuthenticationState {
  switch (action.type) {
    case AuthenticationActionTypes.SIGN_IN:
      return {
        ...state,
        user: action.user,
      };
    case AuthenticationActionTypes.SIGN_OUT:
      return {
        ...state,
        user: null,
      };
    default: {
      return state;
    }
  }
}

export const selectUser = (appState: AppStateWithAuthentication): UserModel => appState.authenticationReducer.user;
export const selectIsAdmin = (appState: AppStateWithAuthentication): boolean =>
  selectUser(appState)?.role === UserRole.Admin;
