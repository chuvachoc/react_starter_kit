import { combineReducers } from "redux";
import { authenticationReducer, popupAlertsReducer } from "./index";

export const rootReducer = combineReducers({
  popupAlertsReducer,
  authenticationReducer,
});
