import { PopupAlertState } from "@src/store/reducers/popupAlertsReducer/popupAlertsReducer.model";
import { SHOW_ALERT } from "./popupAlertsReducer.types";

export class ShowAlert {
  public readonly type = SHOW_ALERT;

  constructor(public content: PopupAlertState) {}
}
