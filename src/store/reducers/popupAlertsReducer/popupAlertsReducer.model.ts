import { PopupAlertType } from "@src/constants";

export interface PopupAlertState {
  variant?: PopupAlertType;
  content: string;
}
