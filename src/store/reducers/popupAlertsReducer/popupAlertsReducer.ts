import { PopupAlertType } from "@src/constants";
import { useDispatch } from "react-redux";
import { SHOW_ALERT } from "./popupAlertsReducer.types";
import { ShowAlert } from "./popupAlertsReducer.actions";
import { PopupAlertState } from "./popupAlertsReducer.model";

interface AppStateWithPopupAlert {
  popupAlertsReducer: PopupAlertState;
}

const initialState = <PopupAlertState>{
  variant: PopupAlertType.Success,
  content: "",
};

export function popupAlertsReducer(state: PopupAlertState = initialState, action: ShowAlert): PopupAlertState {
  switch (action.type) {
    case SHOW_ALERT: {
      return {
        ...initialState,
        ...action.content,
      };
    }
    default: {
      return state;
    }
  }
}

export function usePopupAlertsReducer(): { showAlert: (c: PopupAlertState) => void } {
  const dispatch = useDispatch();

  return {
    showAlert: (content: PopupAlertState) => dispatch(new ShowAlert(content)),
  };
}

export const selectAllPopupAlerts = (state: AppStateWithPopupAlert): PopupAlertState => state.popupAlertsReducer;
