import { IPlainObject } from "@src/models";

interface CustomMiddleware extends IPlainObject {
  withExtraArgument?: () => IPlainObject;
}

function createCustomMiddleware(): CustomMiddleware {
  return () => (next) => (action) => next({ ...action, type: action.type || "" });
}

const customMiddleware = createCustomMiddleware();

customMiddleware.withExtraArgument = createCustomMiddleware;

export default customMiddleware;
