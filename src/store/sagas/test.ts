import { put, takeEvery } from "redux-saga/effects";

const delay = (ms) => new Promise((res) => setTimeout(res, ms));

function* incrementAsync(): Generator {
  yield delay(1000);
  yield put({ type: "INCREMENT" });
}

export function* testSagas(): Generator {
  yield takeEvery("INCREMENT_ASYNC", incrementAsync);
}
