import { all } from "redux-saga/effects";
import { testSagas } from "./test";

export default function* rootSaga(): Generator {
  yield all([testSagas()]);
}
