import { appConfig, DEVELOPMENT_MODE_ENV } from "@src/constants";
import { rootReducer } from "@src/store/reducers/rootReducer";
import { applyMiddleware, compose, createStore, Middleware, Store } from "redux";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./sagas";
import customMiddleware from "./custom-middleware";

const developmentMode = appConfig.env.NODE_ENV === DEVELOPMENT_MODE_ENV;
const composeEnhancers = (developmentMode && window && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

let storeEntity: Store;

function configureStore(): Store {
  const sagaMiddleware = createSagaMiddleware();

  // compose enhancers
  const enhancer = composeEnhancers(applyMiddleware(customMiddleware as Middleware, sagaMiddleware));

  // create store
  const store = createStore(rootReducer, enhancer);

  sagaMiddleware.run(rootSaga);

  if (developmentMode && window.Cypress) {
    window.__CYPRESS_REDUX_STORE__ = store;
  }

  return store;
}

const getStore = (): Store => {
  if (!storeEntity) {
    storeEntity = configureStore();
  }

  return storeEntity;
};

export default getStore;
