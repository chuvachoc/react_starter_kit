import { separators } from "@src/constants";
import { IPlainObject } from "@src/models";

function getClassNamesFromObject(obj: IPlainObject): string {
  return Object.keys(obj)
    .map((key) => (obj[key] ? key : null))
    .filter((v) => v)
    .join(separators.space);
}

export function cn(...args: (string | IPlainObject)[]): string {
  return args
    .map((arg) => {
      if (Array.isArray(arg)) {
        return cn(...arg);
      }

      if (typeof arg === "object") {
        return getClassNamesFromObject(arg);
      }

      return String(arg);
    })
    .filter((v) => v)
    .join(separators.space);
}
