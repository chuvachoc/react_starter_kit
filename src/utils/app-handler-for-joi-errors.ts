import { JoiAppErrorCode } from "@src/constants";
import { ErrorReport } from "joi";
import { pathOr } from "rambda";

export function appHandlerForJoiErrors(errors: ErrorReport[]): ErrorReport[] {
  errors.forEach((err) => {
    const label = pathOr("Unknown Field", ["local", "label"], err);

    switch (err.code) {
      case JoiAppErrorCode.StringBase:
        Object.assign(err, { message: `"${label}" is required` });
        break;
      case JoiAppErrorCode.StringPatternBase:
      case JoiAppErrorCode.StringPatternName:
        Object.assign(err, {
          message: `"${label}" contain an invalid characters`,
        });
        break;
      case JoiAppErrorCode.AnyOnly:
        Object.assign(err, { message: `"${label}" value is not allowed` });
        break;
      default:
    }
  });

  return errors;
}
