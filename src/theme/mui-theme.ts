import { createMuiTheme, Theme } from "@material-ui/core";
import { ZIndex } from "@material-ui/core/styles/zIndex";
import { PaletteModel } from "@src/theme/models";

import palette from "./palette";
import typography from "./typography";
import overrides from "./overrides";
import shadows from "./shadows";

export interface ThemeProps extends Theme {
  palette: PaletteModel;
}

declare module "@material-ui/core/styles/createBreakpoints" {
  interface BreakpointOverrides {
    xs: true;
    sm: true;
    md: true;
    lg: true;
    xl: true;
  }
}
export const muiTheme = createMuiTheme({
  palette,
  typography,
  overrides,
  shadows,
  breakpoints: {
    values: {
      xs: 100,
      sm: 480,
      md: 768,
      lg: 1080,
      xl: 1400,
    },
  },
  zIndex: <ZIndex>{
    appBar: 1200,
    drawer: 1100,
  },
});
