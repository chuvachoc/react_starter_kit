import { Palette, PaletteColor, TypeBackground, TypeText } from "@material-ui/core/styles/createPalette";

interface TypeBackgroundModel extends TypeBackground {
  dark: string;
}

export interface PaletteModel extends Palette {
  black: string;
  white: string;
  background: TypeBackgroundModel;
  text: TypeText;
  success: PaletteColor;
  info: PaletteColor;
  warning: PaletteColor;
  error: PaletteColor;
  icon: string;
  divider: string;
}
