import { Overrides } from "@material-ui/core/styles/overrides";
import MuiButton from "./MuiButton";
import MuiIconButton from "./MuiIconButton";
import MuiPaper from "./MuiPaper";
import MuiTableCell from "./MuiTableCell";
import MuiTableHead from "./MuiTableHead";
import MuiTypography from "./MuiTypography";
import MuiPickersDateRangePickerInput from "./MuiPickersDateRangePickerInput";

export default <Overrides>{
  MuiButton,
  MuiIconButton,
  MuiPaper,
  MuiTableCell,
  MuiTableHead,
  MuiTypography,
  MuiPickersDateRangePickerInput,
};
