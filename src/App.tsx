import { StylesProvider } from "@material-ui/core/styles";
import { ThemeOptions } from "@material-ui/core/styles/createMuiTheme";
import { ThemeProvider } from "@material-ui/styles";
import React, { ReactElement } from "react";
import { BrowserRouter as Router } from "react-router-dom";

import { PopupAlert } from "./components";
import { BASE_ROUTE } from "./constants";
import { Routes } from "./routes/routes";
import { muiTheme } from "./theme";

export function App(): ReactElement {
  return (
    <StylesProvider injectFirst>
      <ThemeProvider<ThemeOptions> theme={muiTheme}>
        <Router basename={BASE_ROUTE}>
          <Routes loading={false} />
        </Router>
        <PopupAlert />
      </ThemeProvider>
    </StylesProvider>
  );
}
