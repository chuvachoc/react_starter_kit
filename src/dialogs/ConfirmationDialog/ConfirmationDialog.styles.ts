import { css, SerializedStyles } from "@emotion/core";

export const useConfirmationDialogStyles = (): SerializedStyles => css`
  .MuiTypography-h6 {
    font-size: 20px;
  }

  .MuiTypography-body1 {
    font-size: 16px;
  }
`;
