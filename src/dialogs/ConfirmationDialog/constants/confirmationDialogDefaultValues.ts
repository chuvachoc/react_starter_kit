const DEFAULT_TITLE = "Confirm action";

const DEFAULT_MESSAGE = "Please confirm your action";

const DEFAULT_CONFIRM_BUTTON_TEXT = "Confirm";

const DEFAULT_CANCEL_BUTTON_TEXT = "Cancel";

export { DEFAULT_TITLE, DEFAULT_MESSAGE, DEFAULT_CONFIRM_BUTTON_TEXT, DEFAULT_CANCEL_BUTTON_TEXT };
