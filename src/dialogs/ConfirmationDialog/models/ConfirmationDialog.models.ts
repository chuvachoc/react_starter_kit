export interface ConfirmationDialogProps {
  loading?: boolean;
  title?: string;
  message?: string;
  confirmButtonText?: string;
  cancelButtonText?: string;
  isOpened: boolean;
  onConfirm: () => void;
  onCancel: () => void;
}
