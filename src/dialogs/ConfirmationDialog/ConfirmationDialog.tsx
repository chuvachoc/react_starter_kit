/** @jsx jsx */
import { jsx } from "@emotion/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { MainLoader } from "@src/components/main-loader";
import React from "react";
import {
  DEFAULT_CANCEL_BUTTON_TEXT,
  DEFAULT_CONFIRM_BUTTON_TEXT,
  DEFAULT_MESSAGE,
  DEFAULT_TITLE,
} from "./constants/confirmationDialogDefaultValues";
import { ConfirmationDialogProps } from "./models/ConfirmationDialog.models";
import { useConfirmationDialogStyles } from "./ConfirmationDialog.styles";

export function ConfirmationDialog(props: ConfirmationDialogProps): React.ReactElement {
  const styles = useConfirmationDialogStyles();

  return (
    <Dialog
      data-test-id="confirmation-dialog"
      css={styles}
      fullWidth={true}
      maxWidth="sm"
      open={props.isOpened}
      scroll="paper"
      aria-labelledby="dialog-title"
      aria-describedby="dialog-description"
    >
      <DialogTitle id="dialog-title" data-test-id="confirmation-dialog-title">
        {props.title ?? DEFAULT_TITLE}
      </DialogTitle>
      <DialogContent>
        {props.loading && <MainLoader fillContainer />}
        <DialogContentText id="dialog-description" data-test-id="confirmation-dialog-message">
          {props.message ?? DEFAULT_MESSAGE}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={props.onCancel} color="primary" autoFocus data-test-id="confirmation-dialog-cancel-button">
          {props.cancelButtonText ?? DEFAULT_CANCEL_BUTTON_TEXT}
        </Button>
        <Button onClick={props.onConfirm} color="primary" data-test-id="confirmation-dialog-confirm-button">
          {props.confirmButtonText ?? DEFAULT_CONFIRM_BUTTON_TEXT}
        </Button>
      </DialogActions>
    </Dialog>
  );
}
