/* eslint-disable no-underscore-dangle */
import getStore from "@src/store";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { App } from "./App";

function startApp(): void {
  const app = (
    <Provider store={getStore()}>
      <App />
    </Provider>
  );

  ReactDOM.render(app, document.querySelector("#app-root"));
}

startApp();
