context('Sign Off', () => {

  beforeEach(() => {
    cy.visit('/');
    cy['login']();
  });

  it('sign-off button should be visible', () => {
    cy.get('[data-test-id="sign-off-button"]').should('be.visible');
    // cy.get('[data-test-id="sign-off-button"]').eq(1).click();
  });

  it('click on sign-off button should log-out', () => {
    cy.url().should('not.include', '/user');

    cy.get('[data-test-id="sign-off-button"]').click();

    cy.url().should('include', '/user');
  });

  it('Check popup after successful sign-off', () => {
    cy.get('[data-test-id="sign-off-button"]').click();

    cy.get('[data-test-id="snackbar-container"]').should('be.visible');
  });
});
