
const tryToLogIn = (code?: string, password?: string) => {
  cy.get('[data-test-id="email-text-field"] input')
    .type(code || 'user-admin', { force: true });

  cy.get('[data-test-id="password-text-field"] input')
    .type(password || '123123', { force: true });

  cy.get('[data-test-id="sign-in-button"]').click();
};

context('Sign In', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('Text checking', () => {
    cy.get('[data-test-id="sign-in-button"]').should('have.text', 'Sign in now');
  });

  it('Check a button', () => {
    cy.get('[data-test-id="sign-in-button"]').then(($button) => {
      expect($button).not.to.be.disabled;
    });
    cy.get('[data-test-id="sign-in-button"]').click().then(($button) => {
      expect($button).to.be.disabled;
    })
  });

  it('Sign in and redirect checking', () => {
    tryToLogIn();
    cy.url().should('not.include', '/user');
  });

  it('Check popup after successful sign in', () => {
    tryToLogIn();

    cy.get('[data-test-id="snackbar-container"]').should('be.visible');
  });

  it('Check behavior with wrong user data', () => {
    tryToLogIn('wrongUser');

    cy.get('[data-test-id="snackbar-container"]').should('be.visible');
    cy.url().should('include', '/user/sign-in');
  });
});
