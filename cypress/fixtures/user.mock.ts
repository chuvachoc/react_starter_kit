export function getUserMock(role: string = "Admin") {
  return {
    companyPositions: [],
    createdAt: "2020-09-01T16:36:09.632Z",
    createdBy: null,
    deletedAt: null,
    email: "example@mail.com",
    firstName: "Name",
    id: "3d8c83e0-ec71-11ea-8980-fb469482ce11",
    isDeleted: false,
    isEmailVerified: true,
    lastName: "LastName",
    lastUpdatedAt: "2020-09-01T16:36:09.632Z",
    lastUpdatedBy: null,
    userRole: {
      role
    }
  }
}
