import { getUserMock } from '../fixtures/user.mock';

Cypress.Commands.add("login", () => {
  cy.request('POST', 'localhost:3001/api/authentication/log-in', {
    code: "user-admin",
    password: "123123"
  }).then(({ body }) => {
    console.log('response - ', body);
    console.log('response - ', cy.window().its('__CYPRESS_REDUX_STORE__').invoke('dispatch', {
      type: "SIGN_IN",
      payload: getUserMock()
    }));
  })
});

Cypress.Commands.add("clearLocalStorage", () => {
  cy.window().its('localStorage').invoke('clear');
});
