#!/usr/bin/env bash
envFile="$PWD/configs/envs/production.env"
env-cmd -f $envFile "$PWD/devops/local/scripts/check-env-vars.sh"

source $envFile

# for low-memory runner we gonna build it sequentially
env-cmd -f $envFile \
    webpack \
        --config ./configs/webpack.config.js \
        --mode production \
        --env.BUILD_ANALYZE=$BUILD_ANALYZE \
        --env.TARGET_CONFIG_IDX=0

env-cmd -f $envFile \
    webpack \
        --config ./configs/webpack.config.js \
        --mode production \
        --env.BUILD_ANALYZE=$BUILD_ANALYZE \
        --env.TARGET_CONFIG_IDX=1
